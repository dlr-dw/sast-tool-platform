# base image
FROM ubuntu:focal
ENV TZ=Europe/Berlin
ENV LANG=C.UTF-8

# label and credits
LABEL \
    name="automated-sast-analysis" \
    author="Tim Sonnekalb <tim.sonnekalb@dlr.de>" \
    contributor_1="Christopher-Tobias Knaust <christopher.knaust@dlr.de>" \
    description="Performing static analysis for git-based repositories."


# Temporary directories for installation files
WORKDIR /tmp/install
COPY . /tmp/install
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update --yes 							     	&& \
    apt-get install --yes --no-install-recommends \
    git \
    zip \
    unzip \
    curl \
    maven \
    python3-pip \
    python3-venv \
    vim \
    cloc

# add spotbugs
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update  --yes                                                && \
	apt-get install --yes                                                \
        software-properties-common                             		     		&& \
    apt-get install --yes --no-install-recommends                        		\
        openjdk-8-jdk-headless

RUN SPOTBUGS_VERSION=4.3.0; \
    curl -fSLO "https://repo.maven.apache.org/maven2/com/github/spotbugs/spotbugs/${SPOTBUGS_VERSION}/spotbugs-${SPOTBUGS_VERSION}.tgz"    							     		&& \
    tar -xzf spotbugs-${SPOTBUGS_VERSION}.tgz 						     	&& \
    mv spotbugs-${SPOTBUGS_VERSION} /usr/local/lib/                                             	&& \
    chmod +x /usr/local/lib/spotbugs-${SPOTBUGS_VERSION}/bin/spotbugs			     		&& \
    ln -s /usr/local/lib/spotbugs-${SPOTBUGS_VERSION}/bin/spotbugs /usr/local/bin/spotbugs     	&& \
    rm spotbugs-${SPOTBUGS_VERSION}.tgz



# add checkstyle
RUN CHECKSTYLE_VERSION=8.88; \
    curl -L https://github.com/checkstyle/checkstyle/releases/download/checkstyle-${CHECKSTYLE_VERSION}/checkstyle-${CHECKSTYLE_VERSION}-all.jar > checkstyle.jar
RUN curl -LO https://github.com/checkstyle/checkstyle/blob/master/config/checkstyle_checks.xml
RUN mkdir /usr/local/lib/checkstyle/ && \
    mv checkstyle.jar /usr/local/lib/checkstyle/
RUN echo "#!/bin/bash" >> /usr/local/lib/checkstyle/checkstyle.sh 		      && \
    echo "java -jar /usr/local/lib/checkstyle/checkstyle.jar \$@" >> /usr/local/lib/checkstyle/checkstyle.sh
RUN chmod +x /usr/local/lib/checkstyle/checkstyle.sh
RUN ln -s /usr/local/lib/checkstyle/checkstyle.sh /usr/local/bin/checkstyle



# add infer
RUN apt-get update --yes && \
    mkdir -p /usr/share/man/man1 && \
    apt-get install --yes --no-install-recommends \
      autoconf \
      automake \
      cmake \
      git \
      libc6-dev \
      libsqlite3-dev \
      opam \
      ocaml \
      openjdk-11-jdk-headless \
      pkg-config \
      python2.7 \
      zlib1g-dev && \
    rm -rf /var/lib/apt/lists/*

# Download the latest Infer release
RUN INFER_VERSION=v1.1.0; \
    cd /opt && \
    curl -L \
      https://github.com/facebook/infer/releases/download/${INFER_VERSION}/infer-linux64-${INFER_VERSION}.tar.xz | \
    tar xJ 										&& \
#    rm -f /infer 									&& \
    ln -s ${PWD}/infer-linux64-$INFER_VERSION /infer
RUN mv /infer /usr/local/lib								&&\
    ln -s /usr/local/lib/infer/bin/infer /usr/local/bin/infer				&&\
    ln -s /usr/local/lib/infer/bin/infer-capture /usr/local/bin/infer-capture 		&&\
    ln -s /usr/local/lib/infer/bin/infer-explore /usr/local/bin/infer-explore 		&&\
    ln -s /usr/local/lib/infer/bin/infer-reportdiff /usr/local/bin/infer-reportdiff	&&\
    ln -s /usr/local/lib/infer/bin/infer-analyze /usr/local/bin/infer-analyze 		&&\
    ln -s /usr/local/lib/infer/bin/infer-compile /usr/local/bin/infer-compile 		&&\
    ln -s /usr/local/lib/infer/bin/infer-run /usr/local/bin/infer-run
ENV ANDROID_HOME=/opt/android-sdk-linux
WORKDIR $ANDROID_HOME
RUN curl -o sdk-tools-linux.zip \
      https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip && \
    unzip sdk-tools-linux.zip && \
    rm sdk-tools-linux.zip
ENV PATH=${ANDROID_HOME}/tools/bin:${PATH}
 
# accept all licenses for sdkmanager
RUN export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre && \
    yes | sdkmanager --licenses


# add PMD
WORKDIR /tmp/install
ENV PMD_VERSION=6.30.0
RUN curl -LO https://github.com/pmd/pmd/releases/download/pmd_releases/${PMD_VERSION}/pmd-bin-${PMD_VERSION}.zip \
      && unzip pmd-bin-${PMD_VERSION}.zip \
      && rm -f pmd-bin-${PMD_VERSION}.zip \
      && mv pmd-bin-${PMD_VERSION} /usr/local/lib/pmd
RUN chmod +x /usr/local/lib/pmd/bin/run.sh
RUN ln -s /usr/local/lib/pmd/bin/run.sh /usr/local/bin/pmd

# install Gradle
ENV GRADLE_VERSION=6.5.1
RUN curl -LO https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip \
      && unzip gradle-${GRADLE_VERSION}-bin.zip \
      && rm -f gradle-${GRADLE_VERSION}-bin.zip \
      && mv gradle-${GRADLE_VERSION} /usr/local/lib/gradle
RUN ln -s /usr/local/lib/gradle/bin/gradle /usr/local/bin/gradle

# install MobileSF
RUN git clone https://github.com/MobSF/Mobile-Security-Framework-MobSF.git \
    && cd Mobile-Security-Framework-MobSF \
    && pip install -r requirements.txt \
    && ./setup.sh \
    && ex -sc 's/$/ --daemon/|w|q' run.sh \
    && ex -sc 's/$/ --daemon/|w|q' scripts/entrypoint.sh \
    && cd ../ \
    && mv Mobile-Security-Framework-MobSF/ /usr/local/lib/MobSF/
RUN ln -s /usr/local/lib/MobSF/run.sh /usr/local/bin/mobsf
# Run MobSF server
RUN cd /usr/local/lib/MobSF/ && ./scripts/entrypoint.sh
RUN cd /usr/local/lib/ && git clone https://github.com/klmmr/mobsfpy.git && cd mobsfpy \
    && pip install . \
    && cd ../ && rm -rf mobsfpy/

# install Flowdroid
RUN curl -LO https://github.com/secure-software-engineering/FlowDroid/releases/download/v2.10/soot-infoflow-summaries-jar-with-dependencies.jar \
      && mkdir /usr/local/lib/flowdroid \
      && mv soot-infoflow-summaries-jar-with-dependencies.jar /usr/local/lib/flowdroid/soot.jar
RUN curl -LO https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip \
      && unzip commandlinetools-linux-7302050_latest.zip \
      && rm commandlinetools-linux-7302050_latest.zip
RUN curl -LO https://raw.githubusercontent.com/secure-software-engineering/FlowDroid/develop/soot-infoflow-android/SourcesAndSinks.txt \
      && mv SourcesAndSinks.txt /usr/local/lib/flowdroid/SourcesAndSinks.txt
RUN echo "#!/bin/sh" >> /usr/local/lib/flowdroid/flowdroid.sh \
      && echo "java -jar /usr/local/lib/flowdroid/soot.jar \$@" >> /usr/local/lib/flowdroid/flowdroid.sh \
      && chmod +x /usr/local/lib/flowdroid/flowdroid.sh
RUN ln -s /usr/local/lib/flowdroid/flowdroid.sh /usr/local/bin/flowdroid

# Install analysis script
RUN python3 -m pip install --upgrade build
RUN python3 -m build
RUN python3 -m pip install --upgrade dist/sast_analysis_pipeline_dlr-1.0.0-py3-none-any.whl

# Post install cleanup
WORKDIR /analysis
RUN apt-get clean
RUN rm -rf /tmp/install
