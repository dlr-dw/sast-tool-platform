# A Static Analysis Platform for Investigating Security Trends in Repositories

This Security Analysis Service for git-based projects based on Static Analysis. This Analysis service has a modular design, so it is easy to extend with other tools and supported programming languages. The analysis is virtualized via Docker. All analysis results are saved in a SQlite database. The results can be vizualized in a dashboard. 

## Publication
This dataset is provided as part of a publication. A citation is always appreciated:
```bibtex
@inproceedings{Sonnekalb2023SASTPlatform,
  author={Sonnekalb, Tim and Knaust, Christopher-Tobias and Gruner, Bernd and Brust, Clemens-Alexander and Heinze, Thomas S. and Kurnatowski, Lynn von and Schreiber, Andreas and Mäder, Patrick},
  booktitle={2023 IEEE/ACM 1st International Workshop on Software Vulnerability (SVM)}, 
  title={A Static Analysis Platform for Investigating Security Trends in Repositories}, 
  year={2023},
  volume={},
  number={},
  pages={1-5},
  doi={10.1109/SVM59160.2023.00005}
  }
```

## SAST Tools
Currently supported SAST tools are:
| Tool | Language | Configuration |
| --- | --- | --- |
| [Flowdroid](./tools/Flowdroid.py) | Android | -s /usr/local/lib/flowdroid/SourcesAndSinks.txt |
| [Infer](./tools/Infer.py) | Java, Android, C, C++, iOS | standard |
| [MobSF](./tools/MobSF.py) | Android, iOS, Windows| standard |
| [PMD](./Pmd.py) | Java, JavaScript | -R rulesets/java/sunsecure.xml |
| [Xanitzer](./tools/Xanitizer.py) (including Spotbugs, FindSecBugs, OWASP dependency check) | Java, Scala, JavaScript, TypeScript | standard |

## Docker
First, build the _Dockerimage_ with `docker build -t sast-analysis:latest - < Dockerfile`. (it needs around 5 GB free space)
Then, you can start the _Dockercontainer_ with `docker run --rm -v $(pwd):/analysis -it sast-analysis:latest` including a shared folder _$(pwd)_ for storing the database.


### Start the SAST analysis
Start the script via `python3 startAnalysis.py`. Here is it's help output:

```
usage: startAnalysis.py [-h] --repo REPO [REPO ...] --language REPOLANGUAGE [REPOLANGUAGE ...] [--data-path DATA_PATH] [--db-path DB_PATH]
                        [--db-schema DB_SCHEMA] [--log {info,debug}]

optional arguments:
  -h, --help            show this help message and exit
  --repo REPO [REPO ...]
                        URL of one or more git remotes
  --language REPOLANGUAGE [REPOLANGUAGE ...]
                        Analyzed programming lanugage(s) (default: None)
  --data-path DATA_PATH
                        Directory holding the local git repository (default: /analysis/security_audit)
  --db-path DB_PATH     path of SQL database (default: sast-analysis.db)
  --db-schema DB_SCHEMA
                        path of schema for creating a new database (default: createDB.sql)
  --log {info,debug}    set a logging level (default: info)

```

### Systemd timer

If you want to run the analysis periodically on your machine:

1. Build the docker image (see below)
2. Copy `sast-analysis.service` and `sast-analysis.timer` to `/etc/systemd/system`
3. Run `systemdctl daemon-reload` (as root)
4. Activate `sast-analysis.timer`

Note that the service file assumes /public to be a writable directory. You can of course adjust both systemd units to your preferences.


### Start the SAST Dashboard
1. Run the analysis. This creates a database called `sast-analysis.db` in the project folder.
2. \[optinoal\] install dependencies: `pip3 install -r requirements.txt`
3. Navigate to the directory `./dashboard/`
4. start the dahsboard server: `python3 dashboard.py`
5. open the browser: [http://127.0.0.1:8050](http://127.0.0.1:8050)
6. \[optional\] deploying the dashboard on a server works with `gunicorn index:server -b :8080`.

## Contributing

* All code must be formatted using [black](https://github.com/psf/black) and [isort](https://pycqa.github.io/isort/).
* To organize your imports, please use `isort --profile black`.

### Extend your own SAST tool

In order to extend this analysis service with another SAST tool and also support other programming languages, you only need to add another python module in the subfolder `./tools/`. The module only needs 4 methods:
 
 - `queryVersion()`: executes the tool to get the tool's version, it returns the version as a string.
 - `init()`: this method initializes the tools properties and configuration and returns a class called `Tool`. Please refer to the other tools modules for its properties.
 - `parse()`: the parsw method opens the tool's report at the defined location and parses it to a list of a list of alerts following the structure `[message, location, severity, runID]`.
 - `updateConfig()`: this method is called to update the tools configuration.
