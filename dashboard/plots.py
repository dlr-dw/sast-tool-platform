#!/usr/bin/env python3
import csv
import textwrap

import matplotlib.pyplot as plt
import numpy as np
import squarify
import argparse
import os
import sqlite3

QUERY_WARNINGS = """SELECT run.snapshot, count(warning.id), snapshot.author_date
    FROM run, snapshot
    LEFT JOIN warning ON run.id = warning.run
    WHERE run.snapshot IN (SELECT snapshot FROM branches WHERE branch = 'origin/HEAD') AND
          run.snapshot IN (SELECT id FROM snapshot WHERE snapshot.repo = 1) AND
          run.snapshot = snapshot.id AND
          run.success = 1 AND
          run.tool IN (SELECT id FROM tool WHERE name = ?)
    GROUP BY run.snapshot
    ORDER BY snapshot.author_date;
    """


QUERY_LOCATION = query = """SELECT location, count(*)
    FROM warning
    WHERE warning.run IN
          (SELECT run.id
           FROM run, branches, snapshot
           WHERE run.tool IN (SELECT id FROM tool WHERE name = ?) AND
                 snapshot.repo = 1 AND
                 snapshot.id = run.snapshot AND
                 branches.branch = 'origin/HEAD' AND
                 branches.snapshot = run.snapshot)
    GROUP BY location
    ORDER BY count(*);
    """


QUERY_MESSAGES = """SELECT message, count(*)
    FROM warning
    WHERE warning.run IN
          (SELECT id FROM run
           WHERE tool IN
                 (SELECT id FROM tool WHERE name =?) AND
                 run.snapshot IN
                 (SELECT id from snapshot WHERE repo = 1))
    GROUP BY message
    ORDER BY count(*);"""


def initDb(dbfile):
    # connect to database
    dbhandler = sqlite3.connect(dbfile)
    return dbhandler


def queryDb(dbhandler, query, tool):
    # query database
    cursor = dbhandler.cursor()
    cursor.execute(query, (tool,))
    results = cursor.fetchall()
    return results


def endDb(dbhandler: sqlite3.Connection) -> None:
    dbhandler.close()


def generateHistoryPlot(results, tool, outputpath):
    # generate plot
    figure, axes = plt.subplots()
    axes.plot([commit[1] for commit in results])
    figure.canvas.draw()
    # modifying x-axis labels, i.e., commit dates
    labels = [item[2][:10] for item in results[::5]]
    axes.set_xticklabels([''] + labels + [''])
    plt.xlabel('Commit Date')
    plt.ylabel('Number of Reports')
    plt.title('%s Reports' % (tool))
    plt.savefig(outputpath + '/%s_history_plot.png' % (tool))
    plt.show()


def generateLocationPlot(results, tool, outputpath):
    squarify.plot(sizes=[location[1] for location in reversed(results)],
                  label=[location[0].split('/')[-1] for location in reversed(results)][:8],
                  alpha=0.6,
                  text_kwargs={'fontsize': 6})
    plt.axis('off')
    plt.title('%s Reports' % (tool))
    plt.savefig(outputpath + '/%s_location_plot.png' % (tool))
    plt.show()


def generateReportPlot(results, tool, outputpath):
    squarify.plot(sizes=[message[1] for message in reversed(results)],
                  label=['\n'.join(textwrap.wrap(message[0], 25))
                         for message in reversed(results)][:7],
                  alpha=0.6,
                  text_kwargs={'fontsize': 6})
    plt.axis('off')
    plt.title('%s Reports' % (tool))
    plt.savefig(outputpath + '/%s_message_plot.png' % (tool))
    plt.show()


def generateBarChartPlot(results, title, outputpath):
    keys = results.keys()
    values = results.values()
    y_pos = np.arange(len(keys))
    plt.bar(y_pos, values, align='center', alpha=0.6)
    plt.xticks(y_pos, keys)
    plt.ylabel('Number')
    plt.title(title)
    plt.xticks(rotation=45)
    plt.subplots_adjust(bottom=0.35)
    plt.savefig(outputpath + '/%s_barplot.png' % (title.replace(" ", "_")))
    plt.show()


def writeCsv(file, array):
    f = open(file, 'w')
    writer = csv.writer(f)
    for row in array:
        writer.writerow(row)
    f.close()


def queryMessages(db, tool):
    dbhandler = initDb(db)
    results = queryDb(dbhandler, QUERY_MESSAGES, tool)
    endDb(dbhandler)
    return results


def queryLocation(db, tool):
    dbhandler = initDb(db)
    results = queryDb(dbhandler, QUERY_LOCATION, tool)
    endDb(dbhandler)
    return results


def queryWarnings(db, tool):
    dbhandler = initDb(db)
    results = queryDb(dbhandler, QUERY_WARNINGS, tool)
    endDb(dbhandler)
    return results


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dbfile",
        help="sqlite db file to be used for generating plots out of it",
        type=str,
        default="/home/sonn_ti/git/sast-db-dumb/sast-analysis.db",
    )
    parser.add_argument(
        "--tool",
        help="specify the tool, if not specified plots of all tools are generated",
        type=str,
        default="all",
    )
    parser.add_argument(
        "--outputfolder",
        help="specify the directory for the plots",
        type=str,
        default="plots",
    )
    parser.add_argument(
        "--plot",
        help="specify the plot type: 'history', 'location' or 'message'",
        type=str,
        default="all",
    )
    args = parser.parse_args()
    dbfile = os.path.realpath(args.dbfile)
    if args.tool == "all":
        tool = ('Infer', 'PMD', 'Xanitizer')
    else:
        tool = args.tool
    outputpath = os.path.realpath(args.outputfolder)
    plot = args.plot

    dbhandler = initDb(dbfile)
    for t in tool:
        if plot == "all" or plot == "history":
            results = queryDb(dbhandler, QUERY_WARNINGS, t)
            writeCsv(outputpath + '/' + t + '_history_data.csv', results)
            generateHistoryPlot(results, t, outputpath)
        if plot == "all" or plot == "location":
            results = queryDb(dbhandler, QUERY_LOCATION, t)
            writeCsv(outputpath + '/' + t + '_location_data.csv', reversed(results))
            generateLocationPlot(results, t, outputpath)
        if plot == "all" or plot == "message":
            results = queryDb(dbhandler, QUERY_MESSAGES, t)
            writeCsv(outputpath + '/' + t + '_message_data.csv', reversed(results))
            generateReportPlot(results, t, outputpath)
    if plot == "all" or plot == "cwe":
        results = queryDb(dbhandler, QUERY_MESSAGES, 'Xanitizer')
        cweResultDict = {}
        cweResultList = []
        for r in results:
            cwe = r[0].split(";")[0]
            amount = r[1]
            if cwe in cweResultDict:
                cweResultDict[cwe] = cweResultDict[cwe] + amount
            else:
                cweResultDict[cwe] = amount
        for key, value in cweResultDict.items():
            cweResultList.append([key, value])
        cweResultList = sorted(cweResultList, key=lambda x: x[1])
        writeCsv(outputpath + '/Xanitizer_messageCWE_data.csv', cweResultList)
        generateBarChartPlot(cweResultDict, "Xanitizer Vulnerability Types", outputpath)
        generateReportPlot(cweResultList, "XanitizerCWE", outputpath)
    if plot == "all" or plot == "Xanitizer":
        results = queryDb(dbhandler, QUERY_MESSAGES, 'Xanitizer')
        subtool = {}
        for r in results:
            plugin = r[0].split(";")[1].split(":")[1]
            if plugin in subtool:
                subtool[plugin] = subtool[plugin] + 1
            else:
                subtool[plugin] = 1
        generateBarChartPlot(subtool, "Xanitizer Alarmtypes", outputpath)
    endDb(dbhandler)
    exit(0)


if __name__ == "__main__":
    main()
