# -*- coding: utf-8 -*-

# Run this app with `python dashboard.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
from dash import dash_table
from dash import dcc
from dash import html
from flask import Flask
import plotly.express as px
import pandas as pd
import sqlite3
from plots import queryMessages, queryLocation, queryWarnings


DATABASE = '../sast-analysis.db'
conn = sqlite3.connect(DATABASE)
cursor = conn.cursor()
cursor.execute('SELECT id, name, url FROM repo')


infer_history_results = queryWarnings(DATABASE, 'Infer')
infer_history = pd.DataFrame(data=infer_history_results, columns=['id', 'Number of Reports', 'Commit Date'])
fig_infer_history = px.line(infer_history, x='Commit Date', y='Number of Reports', title='Infer Reports historically')

infer_location_results = queryLocation(DATABASE, 'Infer')
infer_location = pd.DataFrame(data=infer_location_results, columns=['file', 'amount'])
infer_location['file'] = infer_location['file'].str.rsplit('/', n=1).str[1]
fig_infer_location = px.treemap(infer_location, path=['file'], values='amount', title='Infer Hotspot Files')

infer_message_results = queryMessages(DATABASE, 'Infer')
infer_message = pd.DataFrame(data=infer_message_results, columns=['file', 'amount'])
fig_infer_message = px.treemap(infer_message, path=['file'], values='amount', title='Infer Alarm Types')

pmd_history_results = queryWarnings(DATABASE, 'PMD')
pmd_history = pd.DataFrame(data=pmd_history_results, columns=['id', 'Number of Reports', 'Commit Date'])
fig_pmd_history = px.line(pmd_history, x='Commit Date', y='Number of Reports', title='PMD Reports historically')

pmd_location_results = queryLocation(DATABASE, 'PMD')
pmd_location = pd.DataFrame(data=pmd_location_results, columns=['file', 'amount'])
pmd_location['file'] = pmd_location['file'].str.rsplit('/', n=1).str[1]
fig_pmd_location = px.treemap(pmd_location, path=['file'], values='amount', title='PMD Hotspot Files')

pmd_message_results = queryMessages(DATABASE, 'PMD')
pmd_message = pd.DataFrame(data=pmd_message_results, columns=['file', 'amount'])
fig_pmd_message = px.treemap(pmd_message, path=['file'], values='amount', title='PMD Alarm Types')

xanitizer_history_results = queryWarnings(DATABASE, 'Xanitizer')
xanitizer_history = pd.DataFrame(data=xanitizer_history_results, columns=['id', 'Number of Reports', 'Commit Date'])
fig_xanitizer_history = px.line(xanitizer_history, x='Commit Date', y='Number of Reports',
                                title='Xanitizer Reports historically')

xanitizer_location_results = queryLocation(DATABASE, 'Xanitizer')
xanitizer_location = pd.DataFrame(data=xanitizer_location_results, columns=['file', 'amount'])
xanitizer_location['file'] = xanitizer_location['file'].str.rsplit('/', n=1).str[1]
fig_xanitizer_location = px.treemap(xanitizer_location, path=['file'], values='amount', title='Xanitizer Hotspot Files')

xanitizer_message_results = queryMessages(DATABASE, 'Xanitizer')
xanitizer_message = pd.DataFrame(data=xanitizer_message_results, columns=['file', 'amount'])
fig_xanitizer_message = px.treemap(xanitizer_message, path=['file'], values='amount', title='Xanitizer Alarm Types')

xanitizer_messageCWE_results = queryMessages(DATABASE, 'Xanitizer')
cweResultDict = {}
cweResultList = []
for r in xanitizer_messageCWE_results:
    cwe = r[0].split(";")[0]
    amount = r[1]
    if cwe in cweResultDict:
        cweResultDict[cwe] = cweResultDict[cwe] + amount
    else:
        cweResultDict[cwe] = amount
for key, value in cweResultDict.items():
    cweResultList.append([key, value])
Alarmtypes = {}
for r in xanitizer_messageCWE_results:
    plugin = r[0].split(";")[1].split(":")[1]
    if plugin in Alarmtypes:
        Alarmtypes[plugin] = Alarmtypes[plugin] + 1
    else:
        Alarmtypes[plugin] = 1
xanitizer_messageCWE = pd.DataFrame(data=cweResultDict.items(), columns=['file', 'amount']).sort_values('amount', ascending=False)
fig_xanitizer_messageCWE = px.bar(xanitizer_messageCWE, x='file', y='amount', title="Xanitizer Occurring CWE Types")
xanitizer_alarmtypes = pd.DataFrame(data=Alarmtypes.items(), columns=['file', 'amount']).sort_values('amount', ascending=False)
fig_xanitizer_alarmtypes = px.bar(xanitizer_alarmtypes, x='file', y='amount', title="Xanitizer Occurring CWE Types")

tab_branches = pd.read_sql("SELECT id, branch, snapshot FROM branches", conn)
tab_repo = pd.read_sql("SELECT id, name, url FROM repo", conn)
tab_run = pd.read_sql("SELECT id, snapshot, tool, success FROM run", conn)
tab_tool = pd.read_sql("SELECT id, name, config, version FROM tool", conn)
tab_snapshot = pd.read_sql("SELECT id, committer_date, author_date, commit_message, repo FROM snapshot", conn)
tab_warnings = pd.read_sql("SELECT id, message, location, severity, run FROM warning", conn)


external_css = ["https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css",
                "https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css",
                "https://fonts.googleapis.com/css?family=Raleway:400,300,600",
                "https://cdn.rawgit.com/plotly/dash-app-stylesheets/5047eb29e4afe01b45b27b1d2f7deda2a942311a/goldman-sachs-report.css",
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"]

server = Flask(__name__)

app = dash.Dash(__name__,
                server=server,
                external_stylesheets=external_css)


app.layout = html.Div(children=[
    html.Header(children=
                html.H1(children='SAST DB Dashboard')
                ),

    html.H2(children='SAST Tools'),

    dcc.Tabs(
        id="tabs-tools",
        value='tab-1',
        parent_className='custom-tabs',
        className='custom-tabs-container',
        children=[
            dcc.Tab(label='Infer',
                    className= 'small',
                    children=[
                        dcc.Graph(
                            id='fig_infer_history',
                            figure=fig_infer_history
                        ),
                        dcc.Graph(
                            id='fig_infer_location',
                            figure=fig_infer_location
                        ),
                        dcc.Graph(
                            id='fig_infer_message',
                            figure=fig_infer_message
                        )
                    ]),
            dcc.Tab(label='Xanitizer',
                    children=[
                        dcc.Graph(
                            id='fig_xanitizer_history',
                            figure=fig_xanitizer_history
                        ),
                        dcc.Graph(
                            id='fig_xanitizer_location',
                            figure=fig_xanitizer_location
                        ),
                        dcc.Graph(
                            id='fig_xanitizer_message',
                            figure=fig_xanitizer_message
                        ),
                        dcc.Graph(
                            id='fig_xanitizer_messageCWE',
                            figure=fig_xanitizer_messageCWE
                        ),
                        dcc.Graph(
                            id='fig_xanitizer_alarmtypes',
                            figure=fig_xanitizer_alarmtypes
                        )
                    ]),
            dcc.Tab(label='PMD',
                    children=[
                        dcc.Graph(
                            id='fig_pmd_history',
                            figure=fig_pmd_history
                        ),
                        dcc.Graph(
                            id='fig_pmd_location',
                            figure=fig_pmd_location
                        ),
                        dcc.Graph(
                            id='fig_pmd_message',
                            figure=fig_pmd_message
                        )
                    ]),
        ]),

    html.H2(children='SAST DB Content'),
    dcc.Tabs(
        id="tabs-with-classes",
        children=[
            dcc.Tab(label='Branches',
                    children=[
                        dash_table.DataTable(
                            filter_action='native',
                            id='table_branches',
                            columns=[{"name": i, "id": i} for i in tab_branches.columns],
                            data=tab_branches.to_dict('records')
                        )
                    ]),
            dcc.Tab(label='Runs',
                    children=[
                        dash_table.DataTable(
                            filter_action='native',
                            id='table_run',
                            columns=[{"name": i, "id": i} for i in tab_run.columns],
                            data=tab_run.to_dict('records')
                        )
                    ]),
            dcc.Tab(label='Repositories',
                    children=[
                        dash_table.DataTable(
                            filter_action='native',
                            id='table_repos',
                            columns=[{"name": i, "id": i} for i in tab_repo.columns],
                            data=tab_repo.to_dict('records')
                        )
                    ]),
            dcc.Tab(label='Snapshots',
                    children=[
                        dash_table.DataTable(
                            filter_action='native',
                            id='table_snapshots',
                            columns=[{"name": i, "id": i} for i in tab_snapshot.columns],
                            data=tab_snapshot.to_dict('records')
                        )
                    ]),
            dcc.Tab(label='Tools',
                    children=[
                        dash_table.DataTable(
                            filter_action='native',
                            id='table_tools',
                            columns=[{"name": i, "id": i} for i in tab_tool.columns],
                            data=tab_tool.to_dict('records')
                        )
                    ]),
            dcc.Tab(label='Warnings',
                    children=[
                        dash_table.DataTable(
                            filter_action='native',
                            id='table_warnings',
                            columns=[{"name": i, "id": i} for i in tab_warnings.columns],
                            data=tab_warnings.to_dict('records')
                        )
                    ]),
        ])
])

if __name__ == '__main__':
    app.run_server(debug=True)
