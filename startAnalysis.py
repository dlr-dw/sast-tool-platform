#!/usr/bin/env python3

import argparse
import logging
import os
import shutil
import sys

import tools
import models.Database as Database
from models.Repository import Repo
from util.ToolHelper import startProcess
from util.RepoHelper import findBuildSystem
import util.Cloc

TOOLS = []
for tool in tools.__all__:
    TOOLS.append(__import__('tools.' + tool, locals(), globals(), fromlist=[None, ]))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--repo",
        help="URL of one or more git remotes",
        type=str,
        nargs='+',
        required=True,
        dest="repo"
    )
    parser.add_argument(
        "--language",
        help=f"Analyzed programming lanugage(s) (default: %(default)s)",
        type=str,
        nargs='+',
        required=True,
        dest="repolanguage",
    )
    parser.add_argument(
        "--data-path",
        type=str,
        default=os.path.abspath("security_audit"),
        help="Directory holding the local git repository (default: %(default)s)",
    )
    parser.add_argument(
        "--db-path",
        type=str,
        default="sast-analysis.db",
        help="path of SQL database (default: %(default)s)",
    )
    parser.add_argument(
        "--db-schema",
        type=str,
        default="createDB.sql",
        help="path of schema for creating a new database (default: %(default)s)",
    )
    parser.add_argument(
        "--log",
        type=str,
        default="info",
        choices=["info", "debug"],
        help="set a logging level (default: %(default)s)",
    )
    args = parser.parse_args()
    dataPath = os.path.realpath(args.data_path)
    dbPath = os.path.realpath(args.db_path)
    dbSchema = os.path.realpath(args.db_schema)
    resultDir = os.path.join(dataPath, "analysis_results")
    print(args.repo)
    repoLanguage = [x.lower() for x in args.repolanguage]

    logLevel = logging.INFO if args.log == "info" else logging.DEBUG
    logging.basicConfig(filename="sast-analysis.log", format="%(asctime)s %(message)s", level=logLevel)
    # for Terminal output
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

    conn = Database.initDB(dbPath, dbSchema)
    cursor = conn.cursor()

    for r in args.repo:
        if os.path.isdir(resultDir):
            shutil.rmtree(resultDir)
        os.makedirs(resultDir)

        repo = Repo(ID=None, url=r, language=repoLanguage, targetDir=dataPath)
        buildDir, compileConfig = findBuildSystem(repo.dir, repoLanguage)
        logging.info(f"====> Saving git repo in: {repo.dir}")
        logging.info(f"====> Saving analysis results in: {resultDir}")
        # add repository to sqlite db
        repo.queryID(conn, cursor)
        # initialize all tools
        sastTools = []
        for t in TOOLS:
            sastTools.append(t.init(repo.dir, resultDir, compileConfig))
        # fill the tools table in db
        for t in sastTools:
            t.queryID(conn, cursor)
        os.chdir(buildDir)

        totalRuns = Database.getTotalRuns(cursor, repo, tuple(t.id for t in sastTools))
        count = 1
        for ref in repo.repo.remote().refs:
            commits = list(repo.repo.iter_commits(ref.name))

            for c in commits:
                commitHash = c.hexsha
                # Apparently there isn't a single method to checkout a commit
                repo.repo.head.reference = c
                repo.repo.head.reset(index=True, working_tree=True)
                logging.info(f"====> Processing commit {commitHash}")

                # start compilation process
                compilationReturnCode = startProcess('Compilation', compileConfig)
                logging.info(f"Compilation returned code: {compilationReturnCode}")
                for i, t in enumerate(TOOLS):
                    sastTools[i] = t.updateConfig(sastTools[i])

                size = util.Cloc.runCloc(dataPath, resultDir, repoLanguage)
                logging.info(f"====> Found {size} lines of code to be analyzed")

                Database.snapshotQuery(conn, cursor, c, size, repo.id)
                Database.branchQuery(conn, cursor, ref.name, commitHash)

                for t in sastTools:
                    # if tool needed compiled code for analysis and compilation failed before
                    if t.compilationNeeded[0] == 1 and compilationReturnCode > 0:
                        continue
                    for language in repoLanguage:
                        if language not in t.languages:
                            continue
                        runID = Database.runQuery(conn, cursor, commitHash, t.id)
                        if runID is False:
                            logging.info("Found double Entry")
                            # if: run_id = True, then Entry already exists -> next hash
                            # else: run_id = INT
                            continue
                        returnCode = startProcess(t.name, t.config)
                        logging.info(f'Analysis returned code {returnCode}')
                        if returnCode == 0:  # if tool analysis was successful
                            count = Database.toSQLite(conn, cursor, runID, count, totalRuns, t)
                        else:  # if tool analysis was not successful but was not also not canceled
                            Database.updateEntry(conn, cursor, runID, 2)
        logging.debug(f"count: {count}")
    Database.endDB(conn)


if __name__ == "__main__":
    main()
