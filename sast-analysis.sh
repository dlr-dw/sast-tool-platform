#!/bin/sh

output_directory="/public"
db_name="sast-analysis.db"
base_url="https://github.com/corona-warn-app"
progname="startAnalysis.py"
progargs="--repo ${base_url}/cwa-server java \
	--repo ${base_url}/cwa-testresult-server java \
	--repo ${base_url}/cwa-verification-server java \
	--repo ${base_url}/cwa-verification-portal java \
	--db-path /analysis/${db_name}"
dockercmd="/usr/bin/docker"
container_name="sast-analysis"
dockerargs="run --rm -v ${output_directory}:/analysis ${container_name} ${progname} ${progargs}"

$dockercmd $dockerargs || exit 1
cp ${output_directory}/${db_name} /your/path/for/db
