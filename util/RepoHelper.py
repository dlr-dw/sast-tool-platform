#!/usr/bin/env python3

import os
import logging
from typing import Sequence


def findBuildSystem(repoDir: str, repoLanguage: Sequence[str]) -> (str, str):
    buildFile = ""
    buildDir = ""
    compileConfig = ""
    for root, dirs, files in os.walk(repoDir):
        for file in files:
            if file == "pom.xml":
                buildFile = file
                buildDir = root
                compileConfig = "mvn clean compile -Ddocker.skip=true"
                break
            elif file == "gradlew":
                buildFile = file
                buildDir = root
                if "android" not in repoLanguage:
                    compileConfig = "gradle build -x test"
                else:  # if repo is an Android repo, create the apk file
                    compileConfig = "./gradlew assembleDebug"
                break
    if buildFile != "":
        os.chdir(buildDir)
        logging.info(f"Found build file {buildFile} in directory {buildDir}")
    else:
        os.chdir(repoDir)
        logging.error("ERROR: no build system found in the repository, project cannot be compiled.")
    return buildDir, compileConfig


def findApk(repoDir: str) -> str:
    apk_files = []
    for root, dirs, files in os.walk(repoDir):
        for file in files:
            if file.endswith(".apk"):
                match = os.path.join(root, file)
                logging.info(f"Found apk file under {match}")
                apk_files.append(match)
    if not apk_files:
        return ''
    else:
        return apk_files[0]
