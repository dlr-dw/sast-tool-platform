#!/usr/bin/env python3

import logging
import os.path
import subprocess
from typing import Sequence
import xml.dom.minidom as xml


def parse(report: str, lang: Sequence[str]) -> int:
    logging.debug(f"====> Path to CLOC report: {os.path.realpath(report)}")
    with open(report) as xml_file:
        data = xml.parse(xml_file)
        languages = {}
        for entry in data.getElementsByTagName("language"):
            name = entry.attributes.getNamedItem("name").nodeValue.lower()
            loc = entry.attributes.getNamedItem("code").nodeValue
            languages[name] = loc
        logging.debug(f"====> Found {len(languages)} languages from Cloc report")
        if "android" not in lang:  # Hotfix for Android
            loc = sum([int(languages.get(x.lower(), 0)) for x in lang])
        else:
            loc = sum([int(languages.get("java", 0)), int(languages.get("kotlin", 0))])
        return loc


def runCloc(analysisDir: str, resultDir: str, lang: str) -> int:
    command = f'cloc {analysisDir} --xml --out={resultDir}/cloc_report.xml'
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = process.communicate()
    process.wait()
    if stderr is not None:
        logging.debug(stderr)
    return parse(f'{resultDir}/cloc_report.xml', lang)
