#!/usr/bin/env python3

import subprocess
import logging


def startProcess(processName: str, command: str) -> int:
    """Run specified command on repo"""
    try:
        logging.info(f"====> RUNNING subprocess {processName}")
        logging.debug(f"====> Popen subprocess: {command}")
        cmd_process = subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
            text=True,
        )
        output, errors = cmd_process.communicate()
        for line in output.splitlines():
            logging.debug(line)
        if errors is not None:
            for line in errors.splitlines():
                logging.debug(line)
        # cmd_process.stdout.close()
        # cmd_process.wait()
        return cmd_process.returncode

    except (OSError, subprocess.CalledProcessError) as error:
        logging.error(f"====> Error in {processName}: {error}")
        return 1