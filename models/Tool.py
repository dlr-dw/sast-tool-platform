#!/usr/bin/env python3

import os
import sqlite3
from typing import Any, Callable, List, Optional, Sequence


class Tool:
    def __init__(
        self,
        name: str,
        resultDir: str,
        resultFile: str,
        languages: Sequence[str],
        parser: Callable[[int, str, str], List[Any]],
        compilationNeeded: int,
        startCommand: Optional[str] = None,
        apiKey: Optional[str] = None,
        config: Optional[str] = None,
        version: Optional[str] = None,
    ) -> None:
        self.name = name
        self.resultDir = os.path.join(resultDir, name),
        os.makedirs(os.path.join(resultDir, name), exist_ok=True)
        self.resultFile = os.path.join(resultDir, name, resultFile)
        self.languages = set(languages)
        self.compilationNeeded= compilationNeeded,
        self.startCommand = startCommand
        self.apiKey = apiKey
        self.config = config
        self.version = version

        def parser_(runID: int) -> List[Any]:
            return parser(runID, self.resultFile, self.apiKey)

        self.parser = parser_

    def queryID(self, conn: sqlite3.Connection, cursor: sqlite3.Cursor) -> None:
        """Check if tool ID entry already exists"""
        if self.config is None:
            raise ValueError("Must assign a value to config first")

        cursor.execute(
            "SELECT id FROM tool WHERE name=? AND config=? AND version=?",
            (self.name, self.config, self.version),
        )
        toolID = cursor.fetchone()
        if toolID is None:
            query = "INSERT INTO tool (name, config, version) VALUES (?, ?, ?);"
            cursor.execute(query, (self.name, self.config, self.version))
            self.id = cursor.lastrowid
            conn.commit()
        else:
            # ID found -> returned tuple from DB, id is in [0]
            self.id = toolID[0]
