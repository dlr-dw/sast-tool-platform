#!/usr/bin/env python3

import logging
import os
import sqlite3
import sys
from typing import Optional, Tuple

import git

import models.Tool as Tool
from models.Repository import Repo


def createDB(dbPath: str, schemaPath: str) -> sqlite3.Connection:
    """Create new SQLite database from given schema"""
    if os.path.isfile(dbPath):
        raise FileExistsError(dbPath + " already exists")
    conn = sqlite3.connect(dbPath)
    with open(schemaPath) as schema:
        conn.executescript(schema.read())
    logging.info(f"====> Created database at {dbPath}...")
    return conn


def initDB(dbPath: str, schemaPath: Optional[str] = None) -> sqlite3.Connection:
    """Initialize database and create it if it does not exist"""
    try:
        if os.path.isfile(dbPath):
            conn = sqlite3.connect(dbPath)
        else:
            conn = createDB(dbPath, schemaPath)
        logging.info(f"====> Connected to database at {dbPath}...")
        conn.commit()
        return conn
    except sqlite3.Error as e:
        logging.error(e)
        sys.exit(1)  # exit programm with 1


def endDB(conn: sqlite3.Connection) -> None:
    conn.commit()
    conn.close()
    sys.exit(0)  # end the script


def snapshotQuery(
    conn: sqlite3.Connection,
    cur: sqlite3.Cursor,
    commit: git.objects.commit.Commit,
    size: int,
    repoID: int,
) -> None:
    """Check if hash already exists, create new entry if not"""
    cur.execute("SELECT id FROM snapshot WHERE id=?", (commit.hexsha,))
    snapID = cur.fetchone()
    if not snapID:
        committerDate = commit.committed_datetime.isoformat()
        authorDate = commit.authored_datetime.isoformat()
        query = (
            "INSERT INTO snapshot"
            "(id, committer_date, author_date, commit_message, size, repo)"
            "VALUES (?, ?, ?, ?, ?, ?);"
        )
        cur.execute(
            query,
            (commit.hexsha, committerDate, authorDate, commit.message, size, repoID),
        )
        conn.commit()


def branchQuery(
    conn: sqlite3.Connection, cur: sqlite3.Cursor, branch: str, commit: str
) -> None:
    """Check if branch already exists, create new entry if not"""
    cur.execute(
        "SELECT id from branches where branch=? and snapshot=?;",
        (branch, commit),
    )
    branchID = cur.fetchone()
    # no ID found -> create new one
    if not branchID:
        query = "INSERT INTO branches (branch, snapshot) VALUES (?,?);"
        cur.execute(query, (branch, commit))
        conn.commit()


def runQuery(
    conn: sqlite3.Connection, cur: sqlite3.Cursor, snapID: str, toolID: int
) -> int:
    """Check if run already exists, create new entry if not"""
    query = "SELECT id FROM run WHERE snapshot=? and tool=? and success=0"
    cur.execute(query, (snapID, toolID))
    runID = cur.fetchone()
    # no ID found -> create new one
    if not runID:
        # init success with 0 and refresh later
        query = "INSERT INTO run (snapshot, tool, success) VALUES (?,?,?);"
        cur.execute(query, (snapID, toolID, 0))
        runID = cur.lastrowid
        conn.commit()
        return runID
    else:
        # ID found -> Entry already exists
        # returned bool not int, dirty workaround
        return False


def getTotalRuns(cur: sqlite3.Cursor, repo: Repo, toolIds: Tuple[int, ...]) -> int:
    totalCommits = len(list(repo.repo.iter_commits("--all")))
    query = (
        f"SELECT count(*) FROM run, snapshot "
        f"WHERE run.snapshot = snapshot.id "
        f"AND run.success=1 "
        f"AND snapshot.repo = {repo.id} "
        f"AND tool IN ({','.join('?' * len(toolIds))})"
    )
    cur.execute(
        query,
        toolIds,
    )
    processedRuns = cur.fetchone()[0]
    totalRuns = len(toolIds) * totalCommits - processedRuns
    return totalRuns


def toSQLite(
    conn: sqlite3.Connection,
    cur: sqlite3.Cursor,
    runID: int,
    count: int,
    total: int,
    tool: Tool.Tool,
) -> int:
    try:
        query = "INSERT INTO warning (message, location, severity, is_duplicate, run) VALUES (?,?,?,NULL,?);"
        cur.executemany(query, (tool.parser(runID)))
        # succesfully -> update success from 0 to 1
        cur.execute("UPDATE run SET success=1 WHERE id=?", (runID,))
        conn.commit()  # update succes=1
        logging.info(f"====> {count}/{total} successfully commited!")
        count += 1
    except sqlite3.Error as e:
        logging.error("====> ERROR: ", e)
    finally:
        return count


def updateEntry(
        conn: sqlite3.Connection,
        cur: sqlite3.Cursor,
        runID: int,
        successCode: int
) -> None:
    try:
        cur.execute("UPDATE run SET success=? WHERE id=?", (successCode, runID))
        conn.commit()
    except sqlite3.Error as e:
        logging.error("====> ERROR: ", e)