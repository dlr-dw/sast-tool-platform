#!/usr/bin/env python3

import logging
import os
import sqlite3
import git
from typing import Sequence


class Repo:
    def __init__(self, ID: int, url: str, language: Sequence[str], targetDir: str) -> None:
        self.id = ID
        self.name = os.path.basename(url)
        self.language = language
        self.url = url
        self.gitCheckout(targetDir)

    def queryID(self, conn: sqlite3.Connection, cur: sqlite3.Cursor) -> None:
        """Check if repo ID entry already exists"""
        cur.execute("SELECT id FROM repo WHERE name=? AND url=?", (self.name, self.url))
        repoID = cur.fetchone()
        if not repoID:
            query = "INSERT INTO repo (name, url) VALUES (?, ?);"
            cur.execute(query, (self.name, self.url))
            self.id = cur.lastrowid
            conn.commit()
        else:
            self.id = repoID[0]

    def gitCheckout(self, targetDir: str) -> None:
        """Clones git repo from specified URL and updates it"""
        repoDir = os.path.join(targetDir, self.name)
        try:
            repo = git.Repo(repoDir)
        except:
            repo = git.Repo.clone_from(self.url, repoDir)
            logging.info(f"=============> Cloned repository {self.url}")

        logging.info(f"=============> Using local repository: {repoDir}")
        if repo.head.is_detached:
            repo.heads[0].checkout()
        if repo.is_dirty():
            repo.head.reset(working_tree=True)
        repo.remotes.origin.pull()
        logging.info("=============> Repository is up to date")
        self.repo = repo
        self.dir = repo.working_tree_dir
