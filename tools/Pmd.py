#!/usr/bin/env python3

import json
import logging
import os.path
import re
import subprocess
from typing import Any, List

from models.Tool import Tool


def queryVersion() -> str:
    """Query the installed Pmd version"""
    process = subprocess.Popen("pmd pmd -h", shell=True, stdout=subprocess.PIPE)
    stdoutString = process.communicate()
    match = re.search(r"pmd-bin-(\d*.\d*.\d*)", str(stdoutString))
    if match:
        logging.info(f"====> Using pmd version {match.group(1)}")
        return match.group(1)
    else:
        logging.error("ERROR: could not retrieve the version of tool pmd")
        return "0"


def parse(runID: int, report: str, apiKey: str) -> List[Any]:
    logging.debug(f"====> Path to PMD report: {os.path.realpath(report)}")
    with open(report) as json_file:
        data = json.load(json_file)
        warnings = []
        for entry in data["files"]:
            location = os.path.relpath(entry["filename"])
            for violation in entry["violations"]:
                description = violation["description"]
                severity = violation["priority"]
                warningEntry = [description, location, severity, runID]
                warnings.append(warningEntry)
        logging.debug(f"====> Parsed {len(warnings)} warnings from PMD json file")
        return warnings


def init(repoDir:str, resultDir: str, compileConfig: str) -> Tool:
    t = Tool(
        name="PMD",
        resultDir=resultDir,
        resultFile="pmd_report.json",
        languages=["java", "javascript"],
        compilationNeeded=0,
        parser=parse,
        version=queryVersion(),
    )
    t.config = f'pmd pmd -d {repoDir} -R rulesets/java/sunsecure.xml -failOnViolation false -f json > {t.resultFile}'
    return t


def updateConfig(tool: Tool) -> Tool:
    return tool