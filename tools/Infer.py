#!/usr/bin/env python3
import json
import logging
import os.path
import subprocess
from typing import Any, List

from models.Tool import Tool


def queryVersion() -> str:
    """Query the installed infer version"""
    stdoutString = subprocess.check_output("infer --version-json", shell=True)
    stringToJSON = json.loads(stdoutString.decode("utf-8").strip())
    toolVersion = f'{stringToJSON["major"]}.{stringToJSON["minor"]}.{stringToJSON["patch"]}'
    logging.info(f"====> Using infer version {toolVersion}")
    return toolVersion


def parse(runID: int, report: str, apiKey: str) -> List[Any]:
    logging.debug(f"====> Path to Infer report: {os.path.realpath(report)}")
    with open(report) as json_file:
        data = json.load(json_file)
        warnings = []
        # the json-file starts with a list
        for entry in data:
            severity = entry["severity"]
            location = entry["file"]
            qualifier = entry["qualifier"]
            warningEntry = [qualifier, location, severity, runID]
            warnings.append(warningEntry)
        logging.debug(f"====> Parsed {len(warnings)} warnings from Infer json file")
        return warnings


def init(repoDir: str, resultDir: str, compileConfig: str) -> Tool:
    t = Tool(
        name="Infer",
        resultDir=resultDir,
        resultFile="report.json",
        languages=["java", "android", "c", "c++", "ios"],
        compilationNeeded=0,
        parser=parse,
        version=queryVersion(),
    )
    # cd {repoDir} &&
    t.config = f"infer run --results-dir {t.resultDir[0]} --no-fail-on-issue -- {compileConfig}"
    return t


def updateConfig(tool: Tool) -> Tool:
    return tool