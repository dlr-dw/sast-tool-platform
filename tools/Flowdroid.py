#!/usr/bin/env python3

import json
import logging
import os
import re
import subprocess
from typing import Any, List
from xml.etree import cElementTree as ET

from util.RepoHelper import findApk
from models.Tool import Tool


def queryVersion() -> str:
    """There is no output of the installed Flowdroid version, we need to rely on the Url from the Dockerfile"""
    logging.info(f"====> Using Flowdroid version 2.9")
    return "2.9"


def parse(runID: int, report: str, apiKey: str) -> List[Any]:
    logging.debug(f"====> Path to Flowdroid report: {os.path.realpath(report)}")
    with open(report) as xml_file:
        warnings = []
        location = ""
        severity = ""
        root = ET.parse(xml_file)
        results = root.find('Results').findall('Result')
        for result in results:
            description = str(ET.tostring(result))
            warningEntry = [description, location, severity, runID]
            warnings.append(warningEntry)
        logging.debug(f"====> Parsed {len(warnings)} warnings from Flowdroid xml file")
        return warnings


def init(repoDir: str, resultDir: str, compileConfig: str) -> Tool:
    return Tool(
        name="Flowdroid",
        resultDir=resultDir,
        resultFile="flowdroid_report.xml",
        languages=["android"],
        compilationNeeded=1,
        config = "java -jar /home/projects/sast-analysis/soot.jar -a APK_FILE -p " \
                 "/home/projects/sast-analysis/Android/sdk/platforms/ -s /usr/local/lib/flowdroid/SourcesAndSinks.txt "
                 "-o flowdroid_report.xml -process-multiple-dex",
        parser=parse,
        version=queryVersion(),
    )


def updateConfig(tool: Tool) -> Tool:
    tool.config = f'java -jar /home/projects/sast-analysis/soot.jar -a {findApk(os.getcwd())} ' \
               f"-p /home/projects/sast-analysis/Android/sdk/platforms/ -s /usr/local/lib/flowdroid/SourcesAndSinks.txt " \
               f"-o {tool.resultFile} -process-multiple-dex"
    return tool