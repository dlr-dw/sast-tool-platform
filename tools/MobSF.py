#!/usr/bin/env python3

import logging
import os
import re
import subprocess
from typing import Any, List
from mobsfpy import MobSF
import hashlib

from util.RepoHelper import findApk
from models.Tool import Tool


def getProgramInfo() -> (str, str):
    """Initialize the tool and  query the installed MobSF version"""
    startTool = subprocess.Popen("cd /usr/local/lib/MobSF/ && ./scripts/entrypoint.sh", shell=True,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    startTool.wait()
    stdoutString, stderrString = startTool.communicate()
    match = re.search(r"Mobile Security Framework v(\d*.\d*.\d*)", str(stderrString))
    apiKey = re.search(r"REST API Key: (\w+)", str(stdoutString))
    if match and apiKey:
        logging.info(f"====> Using MobileSF version {match.group(1)}")
        return match.group(1), apiKey.group(1)
    else:
        logging.error("ERROR: could not retrieve the version of tool MobileSF")
        return "0", "0"


def md5(apk_file: str) -> str:
    hash_md5 = hashlib.md5()
    with open(apk_file, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def parse(runID: int, report_file: str, apiKey: str) -> List[Any]:
    mobsf = MobSF(apiKey)
    apk_file = findApk(os.getcwd())
    mobsf.upload(apk_file)
    md5_hash = md5(apk_file)
    # print(mobsf.scan('apk', apk_file, md5_hash))
    report = mobsf.report_json(md5_hash)
    warnings = []
    for warningtype in report["code_analysis"]:
        description = report["code_analysis"][warningtype]['metadata']['cwe'] + "; " + \
                      report["code_analysis"][warningtype]['metadata']['id'] + "; " + \
                      report["code_analysis"][warningtype]['metadata']['description'] + "; OWASP-mobile: " + \
                      report["code_analysis"][warningtype]['metadata']['owasp-mobile'] + "; Type: " + \
                      report["code_analysis"][warningtype]['metadata']['type']
        severity = report["code_analysis"][warningtype]['metadata']['cvss']
        for files in report["code_analysis"][warningtype]:
            for file, line in report["code_analysis"][warningtype][files].items():
                location = file + ',' + line
                warningEntry = [description, location, severity, runID]
                warnings.append(warningEntry)
    logging.info(f"====> Parsed {len(warnings)} warnings from MobileSF json file")
    return warnings


def init(repoDir:str, resultDir: str, compileConfig: str) -> Tool:
    version, apiKey = getProgramInfo()
    return Tool(
        name="MobSF",
        resultDir=resultDir,
        resultFile="mobsf_report.json",
        languages=["android", "ios", "windows"],
        compilationNeeded=1,
        apiKey=apiKey,
        config="mobsf --apikey KEY upload APK-FILE > mobsf_report.json",
        parser=parse,
        version=version,
    )


def updateConfig(tool: Tool) -> Tool:
    tool.config = f"mobsf --apikey={tool.apiKey} upload {findApk(os.getcwd())} > {tool.resultFile}"
    return tool
