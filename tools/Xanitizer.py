#!/usr/bin/env python3
import logging
import os
import re
import subprocess
import xml.dom.minidom as xml
from typing import Any, List

from models.Tool import Tool


def queryVersion() -> str:
    """Query the installed Xanitizer version"""
    process = subprocess.Popen(
        "xanitizer XanitizerConfig", shell=True, stderr=subprocess.PIPE
    )
    stderrString = process.communicate()
    match = re.search(r"version\s(\d*.\d*.\d*)", str(stderrString))
    if match:
        logging.info(f"====> Using Xanitizer version {match.group(1)}")
        return match.group(1)
    else:
        logging.error("====> ERROR: could not retrieve the version of tool Xanitizer")
        return "0"


def parse(runID: int, report: str, apiKey: str) -> List[Any]:
    logging.debug(f"====> Path to Xanitizer report: {os.path.realpath(report)}")
    with open(report) as xml_file:
        data = xml.parse(xml_file)
        warnings = []
        for entry in data.getElementsByTagName("finding"):
            # adds CWE-Number, Producer (either Built-in or Plugin (Spotbugs etc.)), category (p.e. Protocol,
            # Cryptography, etc.) and general description, separated by semicolon
            description = entry.getElementsByTagName("cweNumber")[0].firstChild.data
            description += (
                "; " + entry.getElementsByTagName("producer")[0].firstChild.data
            )
            description += (
                "; " + entry.getElementsByTagName("category")[0].firstChild.data
            )
            description += (
                "; " + entry.getElementsByTagName("description")[0].firstChild.data
            )
            # the location is the relativePath from the directory of Xanitizer
            if (
                entry.getElementsByTagName("node")[0].attributes.getNamedItem(
                    "relativePath"
                )
                is not None
            ):
                location = os.path.relpath(
                    entry.getElementsByTagName("node")[0]
                    .attributes.getNamedItem("relativePath")
                    .nodeValue
                )
            else:
                location = ""
            severity = entry.getElementsByTagName("rating")[0].firstChild.data
            warningEntry = [description, location, severity, runID]
            warnings.append(warningEntry)
        logging.debug(f"====> Parsed {len(warnings)} warnings from Xanitizer xml file")
        return warnings


def init(repoDir:str, resultDir: str, compileConfig: str) -> Tool:
    # intialize Xanitizer with License file once
    process = subprocess.Popen("xanitizer licenseFile=/home/projects/sast-analysis/Xanitizer.license", shell=True,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process.wait()
    t = Tool(
        name="Xanitizer",
        resultDir=resultDir,
        resultFile="xanitizer_report.xml",
        languages=["java", "scala", "javascript", "typescript"],
        compilationNeeded=1,
        parser=parse,
        version=queryVersion(),
    )
    t.config = f"xanitizer rootDirectory={repoDir} findingsListReportOutputFile={t.resultFile} " \
               f"generateDetailsInFindingsListReport=True overwriteConfigFile=True"
    return t


def updateConfig(tool: Tool) -> Tool:
    return tool