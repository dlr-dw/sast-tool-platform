BEGIN TRANSACTION;
CREATE TABLE tool (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    config TEXT NOT NULL, -- the tools' options
    version TEXT NOT NULL
);
CREATE TABLE repo (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    url TEXT NOT NULL
);
CREATE TABLE snapshot (
    id TEXT PRIMARY KEY NOT NULL, -- the commits' hash
    committer_date TEXT NOT NULL,
    author_date TEXT NOT NULL,
    commit_message TEXT NOT NULL,
    size INTEGER NOT NULL, -- LoC of the analyzed language in the commit
    repo INTEGER NOT NULL,
    FOREIGN KEY(repo) REFERENCES repo(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE branches (
    id INTEGER PRIMARY KEY NOT NULL,
    branch TEXT NOT NULL,
    snapshot TEXT NOT NULL,
    FOREIGN KEY(snapshot) REFERENCES snapshot(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE run (
    id INTEGER PRIMARY KEY NOT NULL,
    snapshot TEXT NOT NULL,
    tool INTEGER NOT NULL,
    success INTEGER NOT NULL, -- whether or not running the SAST tool was successful, 2 = analysis ended with error code
    FOREIGN KEY(tool) REFERENCES tool(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY(snapshot) REFERENCES snapshot(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE warning (
    id INTEGER PRIMARY KEY NOT NULL,
    message TEXT NOT NULL,
    location TEXT NOT NULL, -- the file the warning is reported for
    severity TEXT, -- if available 
    is_duplicate INTEGER, --clean database for duplicates
    run INTEGER NOT NULL,
    FOREIGN KEY(run) REFERENCES run(id) ON DELETE CASCADE ON UPDATE CASCADE
);
COMMIT;

